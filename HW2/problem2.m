function [u_att] = problem2()
    % Create new figure
    figure('Name', 'Atrractive Potential Field, Goal at (80,20)')
    % Establish workspace
    [X, Y] = meshgrid(1:100, 1:100);
    % Define Goal
    q_goal = [80;20];
    % Change value of potential constant
    xi = 10^(-3);
    % Define attractive function on the z axis
    u_att = 0.5 * xi * ((X-q_goal(1)).^2 + (Y - q_goal(2)).^2);
    % Plot
    s = surf(X, Y, u_att);
    s.EdgeColor = 'none';
    colorbar
    xlabel('x')
    ylabel('y')
    zlabel('Attractive Potential U_a_t_t')
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
end
