close all
clear
clc
% Call Problem 4 to get combined U
U = problem4();
% Establish workspace
[X Y] = meshgrid(1:100, 1:100);

% New figure
f = figure('Name', 'Non-Holonomic Vehicle Trajectory');
% Replotting field
subplot(1,2,1)
s = surf(X,Y,U);
s.EdgeColor = 'none';
colorbar
xlabel('x')
ylabel('y')
zlabel('Combined Potential Field U_a_t_t + U_r_e_p')
set(gca, 'Xtick', 0:10:100);
set(gca, 'Ytick', 0:10:100);
hold on;

% For quiver Plot
[DX, DY] = gradient(U);
figure(f)
subplot(1,2,2);
quiver(X,Y,-DX,-DY);
xlabel('x');
ylabel('y');
set(gca, 'Xtick', 0:10:100);
set(gca, 'Ytick', 0:10:100);
hold on

% Define Goal and Start
q_goal = [80;20];
start = [10;80];

% Plot goal and start
figure(f)
subplot(1,2,2)
plot(q_goal(1), q_goal(2), 'g*')
plot(start(1), start(2), 'm*')
figure(f)
subplot(1,2,1)
plot3(start(1), start(2), U(start(2),start(1)),'m*')
plot3(q_goal(1), q_goal(2), U(q_goal(2), q_goal(1)),'g*')

% Initial Setup
current = start;
dist_error = sqrt((q_goal(1) - current(1)).^2 + (q_goal(2) - current(2)).^2);
x = current(1);
y = current(2);
theta = pi/2;

% Initial Transformation 
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
transform = [cos(theta) -sin(theta) current(1);
             sin(theta) cos(theta) current(2);
             0 0 1];     
new_car_position = transform * car_position;

% Plot Cars
figure(f)
subplot(1,2,2)
old_car = plot(new_car_position(1,:), new_car_position(2,:), 'r-', 'LineWidth', 0.2);
figure(f)
subplot(1,2,1)
z = U(floor(current(2)),floor(current(1)));
p_old = plot3(new_car_position(1,:), new_car_position(2,:),repmat(z,1,5), 'r-', 'LineWidth', 0.2);

% Actual Algorithm
while(dist_error >= 3)
    dx = -DX(floor(current(2)), floor(current(1))) * 60;
    dy = -DY(floor(current(2)), floor(current(1))) * 60;

    % Angle Correction
    theta_d = atan2(dy,dx);
    theta_error = theta_d - theta;
    theta_error = atan2(sin(theta_error),cos(theta_error));
    % Holonomic Properties
    if(theta_error > (pi/4))
        theta = theta + (pi/4);
        dy = tan(theta) * dx;
    elseif(theta_error < (-pi/4))
        theta = theta - (pi/4);
        dy = tan(theta) * dx;
    else 
        theta = theta_d;
    end

    % Capping velociy to max 
    if((dx^2 + dy^2) > 25)  
       dx = (dx/(dx+dy)) * 5;
       dy = (dy/(dx+dy)) * 5;
    end
    current(1) = current(1) + dx;
    current(2) = current(2) + dy;
    x = [x current(1)];
    y = [y current(2)];

    % Transform car to new pose
    transform = [cos(theta) -sin(theta) current(1);
                 sin(theta) cos(theta) current(2);
                 0 0 1]; 
    new_car_position = transform * car_position;

    % Plot on quiver
    delete(old_car);
    figure(f)
    subplot(1,2,2)
    plot(x,y,'r-', 'Linewidth', 2);
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'r-', 'LineWidth', 0.2);
    drawnow;

    % Plot on 3D
    delete(p_old);
    figure(f)
    subplot(1,2,1)
    z = U(floor(current(2)),floor(current(1)));
    p = plot3(current(1),current(2),z, 'r.', 'Linewidth', 2);
    p_old = plot3(new_car_position(1,:), new_car_position(2,:),repmat(z,1,5), 'r-', 'LineWidth', 0.2);
    drawnow;

    dist_error = sqrt((q_goal(1) - current(1)).^2 + (q_goal(2) - current(2)).^2);
end
