close all
clear
clc
% Call Problem 4 to get combined U
U = problem4();
% Establish workspace
[X Y] = meshgrid(1:100, 1:100);

% New figure
f = figure('Name', 'Vehicle Trajectory');
% Replotting field
subplot(1,2,1)
s = surf(X,Y,U);
s.EdgeColor = 'none';
colorbar
xlabel('x')
ylabel('y')
zlabel('Combined Potential Field U_a_t_t + U_r_e_p')
set(gca, 'Xtick', 0:10:100);
set(gca, 'Ytick', 0:10:100);
hold on;

% For quiver Plot
[DX, DY] = gradient(U);
figure(f)
subplot(1,2,2);
quiver(X,Y,-DX,-DY);
xlabel('x');
ylabel('y');
set(gca, 'Xtick', 0:10:100);
set(gca, 'Ytick', 0:10:100);
hold on

% Define Goal and Start
q_goal = [80;20];
start = [10;80];

% Plot goal and start
figure(f)
subplot(1,2,2)
plot(q_goal(1), q_goal(2), 'g*')
plot(start(1), start(2), 'm*')
figure(f)
subplot(1,2,1)
plot3(start(1), start(2), U(start(2),start(1)),'m*')
plot3(q_goal(1), q_goal(2), U(q_goal(2), q_goal(1)),'g*')

% Actual Algorithm
current = start;
dist_error = sqrt((q_goal(1) - current(1)).^2 + (q_goal(2) - current(2)).^2);
x = current(1);
y = current(2);

while(dist_error >= 2.5)
    % Calculate x and y velocity based on gradient
    dx = -DX(floor(current(2)), floor(current(1))) * 60;
    dy = -DY(floor(current(2)), floor(current(1))) * 60 ;
    % Capping velociy to max 
    if((dx^2 + dy^2) > 25)  
       dx = (dx/(dx+dy)) * 5;
       dy = (dy/(dx+dy)) * 5;
    end
    current(1) = current(1) + dx;
    current(2) = current(2) + dy;
    x = [x current(1)];
    y = [y current(2)];
    % Plot on quiver
    figure(f)
    subplot(1,2,2)
    plot(x,y,'r-', 'Linewidth', 2);
    drawnow;
    % Plot on 3D
    figure(f)
    subplot(1,2,1)
    p = plot3(current(1),current(2),U(floor(current(2)),floor(current(1))), 'r.', 'Linewidth', 2);
    drawnow;
    dist_error = sqrt((q_goal(1) - current(1)).^2 + (q_goal(2) - current(2)).^2);
end
