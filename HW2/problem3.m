function [u_rep] = problem3()
    % Create new figure
    figure('Name', 'Repulsive Potential Field Contour Map')
    % Establish workspace
    [X, Y] = meshgrid(1:100, 1:100);
    hold on;
    % Defining square and finding its center
    square = [30 50 50 30 30; 50 50 70 70 50];
    plot(square(1,:), square(2,:), 'r-', 'Linewidth', 1)
    center = [sum(square(1,1:4))/4; sum(square(2,1:4))/4];
    % Find po which is the farthest point to which the square exists
    % So po is the distance between the center and any point of the square
    po = sqrt((square(1,1)-center(1)).^2 + (square(2,1)-center(2)).^2);
    % Getting P as a function of q (in terms of meshgrid here)
    pq = sqrt((X-center(1)).^2 + (Y-center(2)).^2) ;
    % Constant
    eta = 10^4;
    % Actual calculation
    u_rep = ((1/2) .* eta .* ((1./(pq+po))- (1/(po+po))).^2);
    u_rep(pq > po) = 0;
    % Contour Plot
    contour(X,Y, u_rep)
    colorbar
    xlabel('x')
    ylabel('y')
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
    % 3D Plot
    figure('Name', 'Repulsive Potential Field');
    surf(X,Y, u_rep);
    colorbar;
    xlabel('x');
    ylabel('y');
    zlabel('Repulsive Potential U_r_e_p');
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
end