function [u_rep] = problem3_extra()
	% Create new figure
	figure('Name', 'Repulsive Potential Field');
	% Establish workspace
	[X, Y] = meshgrid(1:1:100, 1:1:100);
    % Defining the square
	sqX = 30:50;
	sqY = 50:70;
    % Constants
	rho_0 = 45;
	eta = 1.5*10^3;
    % Initializing the u_rep matrix
	u_rep = zeros(100,100) *10000;
    % Find distance from each point, to each point in the square
    % Take the max u_rep at every point
    for i = 1:size(sqX,2)
        for j = 1:size(sqY,2)
            rho_q = sqrt((X-sqX(i)).^2 + (Y-sqY(j)).^2); 
            temp = 0.5 * eta* ((1./(rho_q+10))- (1./(rho_0))).^2;	   
            % Find max urep at each point
            u_rep(u_rep < temp) = temp(u_rep < temp); 
        end
    end
    % Plot
	s = surf(X,Y,u_rep);
	s.EdgeColor = 'none';
    colorbar;
    xlabel('x');
    ylabel('y');
    zlabel('Repulsive Potential U_r_e_p');
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
    % Contour Plot
    figure('Name', 'Repulsive Potential Field - Contour Plot');
    contour(X,Y, u_rep)
    colorbar
    xlabel('x')
    ylabel('y')
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
end

