function [U] = problem4()
    % Call problems 2 and 3 to calculate att and rep
    u_att = problem2();
    u_rep = problem3_extra();
    % Establish Workspace
    [X Y] = meshgrid(1:100, 1:100);
    % Combined potential field
    U = u_rep + u_att;
    % Plotting
    figure('Name', 'Combined Potential Field')
    s = surf(X,Y,U);
    s.EdgeColor = 'none';
    colorbar
    xlabel('x')
    ylabel('y')
    zlabel('Combined Potential Field U_a_t_t + U_r_e_p')
    set(gca, 'Xtick', 0:10:100);
    set(gca, 'Ytick', 0:10:100);
end
