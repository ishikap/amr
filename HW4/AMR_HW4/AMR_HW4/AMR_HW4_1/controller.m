function [ u ] = pd_controller(~, s, s_des, params)
%PD_CONTROLLER  PD controller for the height
%
%   s: 2x1 vector containing the current state [z; v_z]
%   s_des: 2x1 vector containing desired state [z; v_z]
%   params: robot parameters

%u = 0;

pos = s(1);
vel = s(2);

pos_des = s_des(1);
vel_des = s_des(2);

e_pos = pos_des - pos;
e_vel = vel_des - vel;


Kp = 412;
Kv = 40;
u = params.mass*(0 + Kp*e_pos + Kv*e_vel + params.gravity);



% FILL IN YOUR CODE HERE


end

