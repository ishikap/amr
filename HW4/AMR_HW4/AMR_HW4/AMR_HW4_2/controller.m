function [ u1, u2 ] = controller(~, state, des_state, params)
%CONTROLLER  Controller for the planar quadrotor
%
%   state: The current state of the robot with the following fields:
%   state.pos = [y; z], state.vel = [y_dot; z_dot], state.rot = [phi],
%   state.omega = [phi_dot]
%
%   des_state: The desired states are:
%   des_state.pos = [y; z], des_state.vel = [y_dot; z_dot], des_state.acc =
%   [y_ddot; z_ddot]
%
%   params: robot parameters

%   Using these current and desired states, you have to compute the desired
%   controls

%u1 = 0;
%u2 = 0;
% FILL IN YOUR CODE HERE

% From params
m = params.mass;
g = params.gravity;
%l = params.arm_length;
I_xx = params.Ixx;

% Z destinations
z_ddot_T = des_state.acc(2);
z_dot_T = des_state.vel(2);
z_T = des_state.pos(2);

% Y destinations
y_ddot_T = des_state.acc(1);
y_dot_T = des_state.vel(1);
y_T = des_state.pos(1);

% Z current state
z_dot = state.vel(2);
z = state.pos(2);

% Y current state
y_dot = state.vel(1);
y = state.pos(1);

% Y current state
phi_dot = state.omega;
phi = state.rot;

% Z controller variables
kv_z = 10;
kp_z = 200;

% Y controller variables
kv_y = 20;
kp_y = 20;

% Y controller variables
kv_phi = 60;
kp_phi = 300;

% Phi_cs
phi_c = (-1/g)*(y_ddot_T + kv_y*(y_dot_T - y_dot) + kp_y*(y_T - y));
%phi_c = pi/6;
phi_c_dot = 0;
phi_c_ddot = 0;

% inputs
u1 = m*(g + z_ddot_T + kv_z*(z_dot_T - z_dot) + kp_z*(z_T - z));
u2 = I_xx*(phi_c_ddot + kv_phi*(phi_c_dot - phi_dot) + kp_phi*(phi_c - phi)); 


end

