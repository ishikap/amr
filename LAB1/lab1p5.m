data = load("map_data.csv");

figure('Name', 'Map');
axis([-0.1 0.1 -0.1 0.1]);
hold on;

x = 0;
y = 0;
plot(x,y,'b.');


for i = 1:size(data,1)
    theta = data(i,2);
    theta = mod(theta,2*pi);
    dx = data(i,1)*cos(theta);
    dy = data(i,1)*sin(theta);
    x = x+dx;
    y = y+dy;
    plot(x,y,'r*');
    drawnow;
    pause(1);
end