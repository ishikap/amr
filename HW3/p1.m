close all; clc; clear;
figure('units','normalized','outerposition',[0 0 1 1])

% Position estimate 
subplot(2,2,1);
axis([0 100 0 100]);
xlabel('x position estimate (m)');
ylabel('y position estimate (m)');
title('Position Estimate');
hold on;
grid on;

% Sensor Measurements along with position estimates
subplot(2,2,3);
axis([0 100 0 100]);
xlabel('x (m)');
ylabel('y (m)');
title('Sensor Measurements');
hold on;
grid on;

% Veocity estimate 
subplot(2,2,2);
xlabel('time (s)');
ylabel('velocity (m/s)');
title('Velocity Estimate');
hold on;
grid on;

% Distance to goal
subplot(2,2,4);
xlabel('time (s)');
ylabel('distance to goal (m)');
title('Distance to goal');
hold on;
grid on;

% Defining and plotting start and goal
start = randi([0 50], 2, 1);
theta = rand();
goal = [start(1)+50*cos(theta) start(2)+50*sin(theta)]';
subplot(2,2,1); 
plot(start(1), start(2), 'ko');
plot(goal(1), goal(2), 'k*');

% Initializing beta variables for PID
v_ref = 3;
Kp = 1;
Ki = 1;
Kd = 0.01;
Ka = 0.15;
dt = 0.25;

% Initializing alpha variables for PID
v = 0;
integral = 0;
derivative = 0;
previous_error = 0;
error = v_ref - v;
v_x = 0;
v_y = 0;
current_angle = 0;
t = 0;

% Initialising beta variables for Kalman
A = 1;
B = dt;
C = [1 1]';
in_noise_var = 1;
Q = (B^2).* in_noise_var; % System Noise
s1_var = 6;
s2_var = 4;
R = [s1_var 0; 0 s2_var]; % Sensor Noises


% Initializing alpha variables for Kalman
x = start(1);
y = start(2);
P = Q;
K = 0;

% Variables for calculating average v estimated
v_total = 0;
v_samples = 0;
v_avg = 0;

% Calculating initial distance to get started
dist_error = sqrt((start(1)-goal(1))^2 +(start(2)-goal(2))^2);

% PID + Kalman Algorithm
while (dist_error > 3)
    
    % PID estimated velocity is fed back into system, 
    % u is input to kalman and is velocity
    v = sqrt(v_x^2 + v_y^2);
    error = v_ref - v;
    integral = integral + error * dt;
    derivative = (error - previous_error)/dt;
    u = (Kp * error) + (Ki * integral) + (Kd * derivative);
    previous_error = error;
    
    % Angle controller
    theta_d = atan2((goal(2)-y),(goal(1)-x));
    theta_error = (theta_d - current_angle);
    gamma = Ka .* atan2(sin(theta_error), cos(theta_error));
    gamma_dt = gamma * dt;
    current_angle = current_angle + gamma_dt;
    
    % find input
    u = u + normrnd(0,1);
    u_x = u*cos(current_angle); 
    u_y = u*sin(current_angle);
    % save off old value
    x_old = x;    
    y_old = y;
    
    % Prediction
    x = A*x + B*u_x;
    y = A*y + B*u_y;
    P = A*P*A' + Q;

    % Get sensor Measurements
    z_x = [x(1); x(1)]+[normrnd(0, sqrt(s1_var)); normrnd(0, sqrt(s2_var))];
    z_y = [y(1); y(1)]+[normrnd(0, sqrt(s1_var)); normrnd(0, sqrt(s2_var))];
    
    % Measurement Update
    K = P*C'*inv((C*P*C') + R);
    x = x + K*(z_x - C*x);
    y = y + K*(z_y - C*y);
    P = (eye(1) - K*C)*P;
    
    % Estimate current velocity by differentiating the position
    v_x = (x(1)-x_old(1))/dt; 
    v_y = (y(1)-y_old(1))/dt; 
    
    % Estimate Distance again
    dist_error = sqrt((x(1)-goal(1))^2 +(y(1)-goal(2))^2);

    % Plot position estimate
    subplot(2,2,1);
    plot(x(1), y(1), 'b.');
    % Plot sensor measurements
    subplot(2,2,3)
    plot(x(1), y(1), 'b.');
    plot(z_x(1), z_y(1), 'r.');
    plot(z_x(2), z_y(2), 'g.');
    lg = legend({'Blue = Estimate ','Red = S1, var = 6', 'Green = S2, var = 4'},'Location','northwest');
    lg.AutoUpdate = 'off';
    % Plot velocity
    subplot(2,2,2)
    plot(t,v,'b.');
    plot(t,u,'r.');
    plot(t,v_avg, 'g.');
    lg = legend({'Blue = Estimated velocity ','Red = Supplied input', 'Green = average estimated velocity'},'Location','northwest');
    lg.AutoUpdate = 'off';
    % Plot distance from goal
    subplot(2,2,4)
    plot(t,dist_error,'g.')
    % Plot in real time
    drawnow;
    
    % Update Time and averaging variables
    t = t+dt;
    v_total = v_total + v;
    v_samples = v_samples + 1;
    v_avg = v_total/v_samples;
end










