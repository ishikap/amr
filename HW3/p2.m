% Creating a new workspace
close all; clc; clear;
f1 = figure('units','normalized','outerposition',[0 0 1 1]);
axis([0 100 0 100]);
xlabel('x (m)');
ylabel('y (m)');
hold on;
grid on;


% Placing Landmarks
landmarks = [25 25; 25 70; 70 25; 70 70; 10 40; 80 60]';
plot(landmarks(1,:), landmarks(2,:), 'ko', 'Linewidth', 3);

% Place car
car = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
theta = rand();
x = randi([25 75]);
y = randi([25 75]);
rotation_matrix = [cos(theta) -sin(theta); sin(theta) cos(theta)];
transformation_start = [rotation_matrix [x; y]; 0 0 1];
new_car = transformation_start * car;


% Create Particles
N = 1000;
particles = randi([1 99], 2, N);
theta_temp = rand(1,N);
particles = [particles;theta_temp]; % last row contains theta

% Estimated Car
estimate = [mean(particles(1,:)) mean(particles(2,:)) mean(particles(3,:))];
transform = [cos(estimate(3)) -sin(estimate(3)) estimate(1);
             sin(estimate(3)) cos(estimate(3)) estimate(2);
             0 0 1];
estimated_car = transform * car;
error = estimate - [x y theta];

% Initialising vectors
error_vec = error;
car_vec = [x y theta];
estimate_vec = estimate;

% Actual Particle Filter algorithm being ran 10 times
for i = 1:10
    % Plot current particles and 
    old_particles = plot(particles(1,:), particles(2,:), 'r.');
    old_car = plot(new_car(1,:), new_car(2,:), 'b-', 'LineWidth', 2);
    old_estimate = plot(estimated_car(1,:),estimated_car(2,:),'g-', 'LineWidth', 2);
    lg = legend({'Black = Landmarks', 'Red = Partciles', 'Blue = Actual Car ', 'Green = Estimated Car'},'Location','northwest');
    lg.AutoUpdate = 'off';
    drawnow;

    % STEP 1: Sampling
    % measuring actual distance (with noise) from robot to landmarks
    measurement = sqrt((x-landmarks(1,:)).^2 + (y-landmarks(2,:)).^2) + normrnd(0,sqrt(8),[1,6]);
    % plot lines from landmark to car (visual purposes)
    old_sensor = plot_measurements(landmarks, x, y, f1);
    
    % STEP 2: Importance Weighting Algorithm
    weights = importance_weight(particles, landmarks, measurement, N);
    
    % STEP 3: Resampling
    particles = resampling(weights, particles,N);
    
    % Movement Update
    % each particle moves forward 1 m and rotates by 0.2 rad
    % rotate by 0.2 rad
    particles(3,:) = particles(3,:) + 0.2 + normrnd(0,sqrt(0.5),[1,N]);
    theta = theta + 0.2 + normrnd(0,sqrt(0.5));
    % move forward by 1m
    v = 2 + normrnd(0,sqrt(0.5));
    particles(1,:) = particles(1,:) + v*cos(particles(3,:));
    particles(2,:) = particles(2,:) + v*sin(particles(3,:));
    v = 2 + normrnd(0,sqrt(0.5));
    x = x + v*cos(theta);
    y = y + v*sin(theta);
    transform = [cos(theta) -sin(theta) x;
                 sin(theta) cos(theta) y;
                 0 0 1];
    new_car = transform * car;
    
    % Estimate the position and calculate the error
    estimate = [mean(particles(1,:)) mean(particles(2,:)) mean(particles(3,:))];
    transform = [cos(estimate(3)) -sin(estimate(3)) estimate(1);
                 sin(estimate(3)) cos(estimate(3)) estimate(2);
                 0 0 1];
    estimated_car = transform * car;
    error = estimate - [x y theta];
    
    % Update vectors
    car_vec = [car_vec; x y theta];
    error_vec = [error_vec; error];
    estimate_vec = [estimate_vec; estimate];   

    pause(0.5)
    
    % Delete old things from the map
    if i~=10
        delete(old_particles);
        delete(old_car);
        delete(old_estimate);
        delete(old_sensor);
    end
end
% Print everything to csv
heading_row = repmat({'actual' 'estimate' 'error'},1, 11);
heading_col = {'x'; 'y'; 'theta'};
temp = repmat({' '}, 3, 33);
heading_col = [heading_col temp];
print_matrix = [' ', heading_row];
print_matrix = [print_matrix; heading_col];
print_matrix(2:4,2:3:end) = num2cell(car_vec');
print_matrix(2:4,3:3:end) = num2cell(estimate_vec');
print_matrix(2:4,4:3:end) = num2cell(error_vec');
disp(print_matrix);

% Printing the error
error_norm = sqrt(error_vec(:,1).^2 + error_vec(:,2).^2);
figure;
xlim([1 10]);
xlabel('iterations');
ylabel('error (m)');
hold on;
plot(0:10,error_norm,'r-');


% R : plot sensor measurements
% M : the figure, takes in current position and known landmarks 
% E : returns the objects that were plotted so that it can be deleted later
function [old_sensor] = plot_measurements(landmarks, x, y, f1)
    old_sensor = [];
    for i = 1:size(landmarks,2)
        figure(f1)
        sensor = plot([landmarks(1,i) x], [landmarks(2,i) y], 'y-');
        old_sensor = [old_sensor sensor];
    end
    lg = legend({'Black = Landmarks', 'Red = Partciles', 'Blue = Actual Car ', 'Green = Estimated Car', 'Yellow = Sensor Measurements'},'Location','northwest');
    lg.AutoUpdate = 'off';
end

% R : Get normalized weights
% M : none, takes particles, landmarks and sensor meaturements
% E : retuns normalized weights of each particle
function [weights] = importance_weight(particles, landmarks, measurement, N)
    weights = ones(1,N);
    for i = 1:size(landmarks,2)
        dist = sqrt((particles(1,:)-landmarks(1,i)).^2 + (particles(2,:)-landmarks(2,i)).^2);
        weights = weights .* normpdf(dist,measurement(i),sqrt(8));
    end
    % normalize weights
    sum_weights = sum(weights);
    weights = weights./sum_weights;
end

% R : Randomly pick 1000 new particles depending on the wieghts
% M : none, get's weights, particles
% E : returns new set of particles, assigned to old particles
function [particles_new] = resampling(weights, particles,N)
    particles_new = zeros(3,N);
    index = randi([1 N]);
    b = 0;
    for i = 1:N
        b = b + 2*max(weights)*rand();
        while(weights(index) < b)
           b = b - weights(index);
           index = mod(index,N)+1;
        end
        particles_new(:,i) = particles(:, index);
    end
end




