%% 5. B) Follow a circle: draw a circle of radius r = 75m 
% in the center of the workspace and 
% make the robot follow it from any starting point of the workspace. 
% Plot the position and velocities of the robots
close all
clear 
clc
%figure('Name', 'Problem5B - Cartesian', 'NumberTitle', 'off', 'Units', 'centimeters');
subplot(1,2,1)
axis([0 200 0 200]);
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
xlabel('x')
ylabel('y')
grid on;
hold on;

subplot(1,2,2)
%figure('Name', 'Problem5B - Velocity', 'NumberTitle', 'off', 'Units', 'centimeters');
hold on 
grid on
xlabel('time (seconds)')
ylabel('velocity (metres/second)')

% Generating random angle
theta_initial = rand();
% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Transformation
rotation_matrix = [cos(theta_initial) -sin(theta_initial); sin(theta_initial) cos(theta_initial)];
transformation_start = [rotation_matrix [100; 100]; 0 0 1];
new_car_position = transformation_start * car_position;
% Plot
subplot(1,2,1)
old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 2);

r = 75;
x = 100;
y = 100;
th = 0:pi/32:2*pi;

circle =[r * cos(th) + x; r * sin(th) + y];
subplot(1,2,1)
plot(circle(1,:), circle(2,:),'r-');



% Setting initials
current = [100;100];
v_ref = 3;
v = 0;
integral = 0;
error = v_ref - v;
current_g = circle(:,2);
dt = 0.5;
acc = 0;
Kp = 1;
Ki = 0.5;
x = [];
y = [];
v_vector = v;
t = 0;
Kh = 0.5;
current_angle = theta_initial;
dist_error = sqrt(((current_g(2)-current(2)).^2) + ((current_g(1)-current(1)) .^2));
visited = 1;

while( current_g ~= circle(:,end))
   x = [x current(1)];
   y = [y current(2)];
   subplot(1,2,1)
   plot(current(1), current(2), 'g.')
   drawnow
   %error = v_ref - v;
   error = dist_error-2;
   integral = integral + error * dt;
   v = (Kp * error) + (Ki * integral);
   
   %v = v + (acc * dt);
   if(v > 5) 
      v = 5; 
   end
   v_vector = [v_vector v];
   subplot(1,2,2)
   plot(t*dt, v, 'b.')
   drawnow
   
   theta_d = atan2((current_g(2)-current(2)),(current_g(1)-current(1)));
   theta_error = (theta_d - current_angle);
   gamma = Kh .* atan2(sin(theta_error), cos(theta_error));
   gamma_dt = gamma*dt;
    
   if (gamma_dt > pi/4)
       gamma_dt =  pi/4;
   elseif(gamma_dt < -pi/4)
        gamma_dt = -pi/4;
   end 
    current_angle = current_angle + gamma_dt;
    current(1) = current(1) + v * cos(current_angle) * dt;
    current(2) = current(2) + v * sin(current_angle) * dt;
    
    transform = [cos(current_angle) -sin(current_angle) current(1);
                 sin(current_angle) cos(current_angle) current(2);
                 0 0 1];
    new_car_position = transform * car_position;
    delete(old_car)
    subplot(1,2,1)
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 0.2);   
    drawnow
    
    dist_error = sqrt(((current_g(2)-current(2)).^2) + ((current_g(1)-current(1)) .^2));
    
    if(dist_error < 5) 
       visited = visited + 1;
       if(current_g ~= circle(:,end))
          current_g = circle(:, visited+1);
       end
    end
   t = t + 1;
end

%plot(x, y , 'g-');


t = 0:t;
subplot(1,2,2)
plot(t*dt, v_vector, 'b-');






