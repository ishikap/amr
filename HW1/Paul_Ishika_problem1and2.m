close all
clear 
clc

%% Implement a non holonomic vehicle in Matlab with the following specs
%  max steering angle = pi/4
%  max speed = 5 m/s
%  workspace dimension 200m x 200m. The global frame is at (0,0)
%  create and represent a desired shape for your robot (triangle, rectangle, …)
%  The robot’s initial configuration is (100, 100, rand(θ))


%% Setting initial position of car
figure('Name', 'Problem1');
axis([0 200 0 200]);
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
xlabel('x');
ylabel('y');
grid on;
hold on;
% Generating random angle
theta_initial = rand();
% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Transformation
rotation_matrix = [cos(theta_initial) -sin(theta_initial); sin(theta_initial) cos(theta_initial)];
transformation_start = [rotation_matrix [100; 100]; 0 0 1];
car_position = transformation_start * car_position;
% Plot
plot(car_position(1,:), car_position(2,:), 'b-', 'LineWidth', 2);



% 1. Implement a homogeneous transformation 
% that translates the robot by a vector (#_letter of your name,
% #_letter of your family name) (e.g. A=1, B=2, C=3, etc.) and rotate 
% by (pi/(5 + #_group). Plot the initial
% configuration and the final configuration in the workspace.

% Letter of name = I = 9; Letter of last name = P = 16
% Translate by (9,16)
% Group number = 3
% Rotate by (pi/8)

% If first rotate then translate 
theta_problem1 = pi/8;
rotation_matrix = [cos(theta_problem1) -sin(theta_problem1); sin(theta_problem1) cos(theta_problem1)];
transformation_problem1 = [rotation_matrix [9; 16];0 0 1];
% car position with respect to car's reference frame
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% transform car to new position with respect to car's reference frame
car_position = transformation_problem1 * car_position;
% convert new position to gloabl reference frame
new_car_position = transformation_start * car_position;
plot(new_car_position(1,:), new_car_position(2,:), 'r-', 'LineWidth', 2);
legend({'Blue = initial position','Red = position after transformation'},'Location','northeast');

%% 2. Go to goal. 
% In this simulation pick a random goal point in the environment and drive the robot to the point.
% The initial pose of the robot is as calculated in 1. 
% The robot's initial and final velocity have to be 0. 
% Plot x and y position, and velocities of the robot with respect to time.

%f2_xy = figure('Name', 'Problem2 - Cartesian','Units', 'centimeters');
subplot(1,2,1);
axis([0 200 0 200]);
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
xlabel('x');
ylabel('y');
grid on;
hold on;
old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 2);


%f2_vel = figure('Name', 'Problem2 - Velocity','Units', 'centimeters');
subplot(1,2,2)
xlabel('time (seconds)');
ylabel('velocity (metres/second)');
hold on 
grid on

P = [randi([0, 200]); randi([0, 200])];
%figure(f2_xy)
subplot(1,2,1)
plot(P(1),P(2), 'r*');


car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% find current pose and current angle
transformation_problem2 = transformation_start * transformation_problem1;
current_angle = theta_initial + theta_problem1;
current = transformation_problem2(1:2, 3);
x = [];
y = [];
error = sqrt(((P(2)-current(2)).^2) + ((P(1)-current(1)) .^2));
K_v = 0.5;
K_p = 0.5;
dt = 0.2;
v = 0;
t = 0;
%figure(f2_vel)
subplot(1,2,2)
plot(t*dt, v, 'b.')
t = 1;
v_vector = v;
while (error > 0.01)
    x = [x current(1)];
    y = [y current(2)];
    
    %figure(f2_xy)
    subplot(1,2,1)
    plot(current(1), current(2), 'g.', 'Linewidth', 0.5)
    drawnow
    
    
    
    v = K_v * sqrt(((P(2)-current(2)).^2) + ((P(1)-current(1)).^2));
    if (v>5) 
        v = 5; 
    end
    v_vector = [v_vector v];

    %figure(f2_vel)
    subplot(1,2,2)
    plot(t*dt, v, 'b.')
    drawnow
    theta_d = atan2((P(2)-current(2)),(P(1)-current(1)));
    theta_error = (theta_d - current_angle);
    gamma = K_p .* atan2(sin(theta_error), cos(theta_error));
    
    gamma_dt = (gamma * dt);
    if (gamma_dt > pi/4)
       gamma_dt =  pi/4;
    elseif(gamma_dt < -pi/4)
        gamma_dt = -pi/4;
    end 
    
    current_angle = current_angle + gamma_dt;
    current(1) = current(1) + v * cos(current_angle) * dt;
    current(2) = current(2) + v * sin(current_angle) * dt;
    
    delete(old_car); 

    transform = [cos(current_angle) -sin(current_angle) current(1);
                 sin(current_angle) cos(current_angle) current(2);
                 0 0 1]; 
    
    new_car_position = transform * car_position;
    
    %figure(f2_xy)
    subplot(1,2,1)
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 0.2);
    drawnow;
    error = sqrt(((P(1)-current(1)).^2) + ((P(1)-current(1)) .^2));
    t = t + 1;
    
end

t = 0:t-1;

%figure(f2_vel)
subplot(1,2,2)
plot(t*dt, v_vector, 'b-');




