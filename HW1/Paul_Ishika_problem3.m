%% 3. Implement a cruise controller (i.e., PID) with vref = 3 m/s. 
% Make the robot go through 3 random points in the environment 
% and switch to next goal once the current goal has been visited. 
% Plot the velocity of the robot.
close all
clear 
clc
%figure('Name', 'Problem3 - Cartesian', 'NumberTitle', 'off', 'Units', 'centimeters');
subplot(1,2,1)
axis([0 200 0 200]);
xlabel('x')
ylabel('y')
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
grid on;
hold on;

subplot(1,2,2)
xlabel('time (seconds)')
ylabel('velocity (metres/second)')
%figure('Name', 'Problem3 - Velocity', 'NumberTitle', 'off', 'Units', 'centimeters');
hold on 
grid on

% Generating random angle
theta_initial = rand();
% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Transformation
rotation_matrix = [cos(theta_initial) -sin(theta_initial); sin(theta_initial) cos(theta_initial)];
transformation_start = [rotation_matrix [100; 100]; 0 0 1];
new_car_position = transformation_start * car_position;
% Start position of car
subplot(1,2,1)
old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 2);

% Establish goals
goals = randi([0,200], 2, 3);
subplot(1,2,1)
plot(goals(1, :),goals(2, :), 'r*');

% Setting initials
current = [100;100];
v_ref = 3;
v = 0;
integral = 0;
derivative = 0;
previous_error = 0;
error = v_ref - v;
current_g = goals(:,1);
visited = 0;
dt = 0.5;
acc = 0;
Kp = 0.2;
Ki = 0.009;
Kd = 0.001;
x = [];
y = [];
v_vector = v;
t = 0;
K_p = 0.5;
current_angle = theta_initial;
subplot(1,2,2)
plot(t*dt, v, 'b.')
t = 1;
while(visited < 3)
   x = [x current(1)];
   y = [y current(2)];
   subplot(1,2,1)
   plot(current(1), current(2), 'g.')
   drawnow
   error = v_ref - v;
   integral = integral + error * dt;
   derivative = (error - previous_error)/dt;
   acc = (Kp * error) + (Ki * integral) + (Kd * derivative);
   previous_error = error;
   
   v = v + (acc * dt); 
   if(v > 5) 
      v = 5; 
   end
   
   subplot(1,2,2)
   plot(t*dt, v, 'b.')
   drawnow
   
   v_vector = [v_vector v];
   theta_d = atan2((current_g(2)-current(2)),(current_g(1)-current(1)));
   theta_error = (theta_d - current_angle);
   gamma = K_p .* atan2(sin(theta_error), cos(theta_error));
   gamma_dt = gamma * dt;
   if (gamma_dt > pi/4)
       gamma_dt =  pi/4;
   elseif(gamma_dt < -pi/4)
        gamma_dt = -pi/4;
   end 
    current_angle = current_angle + gamma_dt;
    current(1) = current(1) + v * cos(current_angle) * dt;
    current(2) = current(2) + v * sin(current_angle) * dt;
    
    delete(old_car)
    transform = [cos(current_angle) -sin(current_angle) current(1);
                 sin(current_angle) cos(current_angle) current(2);
                 0 0 1];
    new_car_position = transform * car_position;
    subplot(1,2,1)
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 0.2);   
    drawnow
             
    
    dist_error = sqrt(((current_g(2)-current(2)).^2) + ((current_g(1)-current(1)) .^2));
    
    if(dist_error < 3) 
       visited = visited + 1;
       if(visited < 3)
          current_g = goals(:, visited+1);
       end
    end
   t = t + 1;
end



t = 0:t-1;
subplot(1,2,2)
plot(t*dt, v_vector, 'b-');


