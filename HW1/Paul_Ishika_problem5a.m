%% 5. A) Move to pose: robot starts in position (0,0,0) 
% and needs to go to (130+ (#_letter of your name, 
% 130+ #_letter of your family name, pi/(5 + #_group). 
% Plot the initial pose, trajectory, and final pose
close all
clear 
clc
figure('Name', 'Problem5A - Cartesian', 'NumberTitle', 'off', 'Units', 'centimeters');
axis([0 200 0 200]);
xlabel('x')
ylabel('y')
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
grid on;
hold on;


% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Start position of car
old_car = plot(car_position(1,:), car_position(2,:), 'b-', 'LineWidth', 2);

% plot the pose
plot(0:25, zeros(1,26), 'r-');
plot(zeros(1,26), 0:25, 'r-');

% letter of name I = 9, letter of family name P = 16, group number 3
% new pose = (139, 146, pi/8)
P = [139;146];
current = [0;0];
current_angle = 0;

x = [];
y = [];
dist_error = sqrt(((P(2)-current(2)).^2) + ((P(1)-current(1)) .^2));
theta_error = pi/8;
K_p = 0.15;
K_b = -0.01;
K_a = 0.18;
dt = 0.5;
v = 0;
t = 0;
v_vector = v;
while (dist_error > 0.1 || abs(theta_error) > 0.1)
    x = [x current(1)];
    y = [y current(2)];
    plot(current(1), current(2), 'g.')
    v = K_p * sqrt(((P(2)-current(2)).^2) + ((P(1)-current(1)).^2));
    if (v>5) 
        v = 5;
    end
    v_vector = [v_vector v];
    
    alpha = atan2((P(2)-current(2)),(P(1)-current(1))) - current_angle;
    beta = current_angle - alpha;
    
    gamma = (K_a * alpha) + (K_b * beta);
    gamma_dt = gamma * dt;
    
    if (gamma_dt > pi/4)
       gamma_dt =  pi/4;
    elseif(gamma_dt < -pi/4)
        gamma_dt = -pi/4;
    end 
    
    current_angle = current_angle + gamma_dt;
    current(1) = current(1) + v * cos(current_angle) * dt;
    current(2) = current(2) + v * sin(current_angle) * dt;

    transform = [cos(current_angle) -sin(current_angle) current(1);
                 sin(current_angle) cos(current_angle) current(2);
                 0 0 1]; 
    
    new_car_position = transform * car_position;
    delete(old_car)
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'm-', 'LineWidth', 0.5);
    
    drawnow
    
    dist_error = sqrt(((P(1)-current(1)).^2) + ((P(1)-current(1)) .^2));
    theta_error = (pi/8) - current_angle;
    t = t + 1;
end

plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 0.5);
% Plot new pose
x = 139:164;
m = tan(pi/8);
y = m*x + 88.42;
plot(x,y,'r-');

y = 146:171;
m = -1/m;
x = (y-482)/m;
plot(x,y,'r-');