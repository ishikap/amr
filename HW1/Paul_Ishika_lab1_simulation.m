hz = 10;
vel_ref = 0.3;
vel_error = 0.3;
prev_vel_error = 0;
Kp = 0.06;
Ki = 0.4;
Kd = 0.003;
integral = 0;
derivative = 0;
output = 0;
v = 0;
t = 0;
while (vel_error > 0.001)
   vel_error = vel_ref - output;
   integral = integral + vel_error/hz;
   derivative = (vel_error - prev_vel_error)*hz;
   output = Kp*vel_error + Ki*integral + Kd*derivative;
   prev_vel_error = vel_error;
   v = [v output];
   t = t + 1;
end
t = 0:1/hz:t/hz;
plot(t, v)
grid on; 