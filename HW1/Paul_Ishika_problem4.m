%% 4. Follow a wall: 
% Draw a line in the workspace and make the robot follow the line 
% at a constant distance (10m) from the line.
% Plot the relative distance between robot and line over time
close all
clear 
clc
subplot(1,2,1)
%figure('Name', 'Problem4 - Cartesian', 'NumberTitle', 'off', 'Units', 'centimeters');
axis([0 200 0 200]);
xlabel('x');
ylabel('y');
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
grid on;
hold on;

subplot(1,2,2)
xlabel('time (seconds)');
ylabel('distance from wall (metres)')
%figure('Name', 'Problem4 - Relative Distance', 'NumberTitle', 'off', 'Units', 'centimeters');
hold on 
grid on


% Generating random angle
theta_initial = rand();
% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Transformation
rotation_matrix = [cos(theta_initial) -sin(theta_initial); sin(theta_initial) cos(theta_initial)];
transformation_start = [rotation_matrix [100; 100]; 0 0 1];
new_car_position = transformation_start * car_position;
% Start position of car
subplot(1,2,1)
old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 2);


% Draw a line 
x = 0:1:200;
a = -2; 
b = 10;
c = -300;
y = (-a/b) * x + (-c/b);
plot(x, y);


% Setting initials
current = [100;100];
v_ref = 3;
v = 0;
integral = 0;
derivative = 0;
previous_error = 0;
error = v_ref - v;
dt = 0.5;
acc = 0;
Kp = 0.2;
Ki = 0.009;
Kd = 0.001;
x = [];
y = [];
d_vector = [];
t = 0;
current_angle = theta_initial;
Kt = 0.02;
Kh = 0.3;



while(current(1) > 1.5 && current(1) < 198.5 && current(2) > 1.5 && current(2) < 198.5 )
   x = [x current(1)];
   y = [y current(2)];
   subplot(1,2,1)
   plot(current(1), current(2), 'g.')
   drawnow
   error = v_ref - v;
   integral = integral + error * dt;
   derivative = (error - previous_error)/dt;
   acc = (Kp * error) + (Ki * integral) + (Kd * derivative);
   previous_error = error;
   
   v = v + (acc * dt);
   if(v > 5)
      v = 5; 
   end
  
   
   d = (a*current(1) + b*current(2) + c)/ sqrt((a ^2) + (b^2));
   d_vector = [d_vector d];
   subplot(1,2,2)
   plot(t*dt, d, 'b.');
   drawnow
   
   a_t = -Kt * (d-10);
   theta_d = atan(-a/b);
   theta_error = (theta_d - current_angle);
   a_h = Kh .* theta_error;
   gamma = a_t + a_h;
   gamma_dt = gamma*dt; 
    if (gamma_dt > pi/4)
        gamma_dt =  pi/4;
    elseif(gamma_dt < -pi/4)
        gamma_dt = -pi/4;
    end 
    current_angle = current_angle + gamma_dt;
    current(1) = current(1) + v * cos(current_angle) * dt;
    current(2) = current(2) + v * sin(current_angle) * dt;
    transform = [cos(current_angle) -sin(current_angle) current(1);
                 sin(current_angle) cos(current_angle) current(2);
                 0 0 1];
    new_car_position = transform * car_position;
    delete(old_car)
    subplot(1,2,1)
    old_car = plot(new_car_position(1,:), new_car_position(2,:), 'b-', 'LineWidth', 0.2);   
    drawnow
   t = t + 1;
end

%plot(x, y , 'g-');

t = 0:(t-1);
subplot(1,2,2)
plot(t*dt, d_vector, 'b-');