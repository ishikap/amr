close all
clear 
clc

%% Implement a non holonomic vehicle in Matlab with the following specs
%  max steering angle = pi/4
%  max speed = 5 m/s
%  workspace dimension 200m x 200m. The global frame is at (0,0)
%  create and represent a desired shape for your robot (triangle, rectangle, …)
%  The robot’s initial configuration is (100, 100, rand(θ))


%% Setting initial position of car
figure('Name', 'Problem1');
axis([0 200 0 200]);
set(gca, 'Xtick', 0:10:200);
set(gca, 'Ytick', 0:10:200);
grid on;
hold on;
% Generating random angle
theta_initial = rand();
% Let's start by assuming that car is centered at (0,0)
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% Transformation
rotation_matrix = [cos(theta_initial) -sin(theta_initial); sin(theta_initial) cos(theta_initial)];
transformation_start = [rotation_matrix [100; 100]; 0 0 1];
car_position = transformation_start * car_position;
% Plot
plot(car_position(1,:), car_position(2,:), 'b-', 'LineWidth', 2);



% 1. Implement a homogeneous transformation 
% that translates the robot by a vector (#_letter of your name,
% #_letter of your family name) (e.g. A=1, B=2, C=3, etc.) and rotate 
% by (pi/(5 + #_group). Plot the initial
% configuration and the final configuration in the workspace.

% Letter of name = I = 9; Letter of last name = P = 16
% Translate by (9,16)
% Group number = 3
% Rotate by (pi/8)

% If first rotate then translate 
theta_problem1 = pi/8;
rotation_matrix = [cos(theta_problem1) -sin(theta_problem1); sin(theta_problem1) cos(theta_problem1)];
transformation_problem1 = [rotation_matrix [9; 16];0 0 1];
% car position with respect to car's reference frame
car_position = [-2 -2 2 2 -2; -1 1 1 -1 -1; 1 1 1 1 1];
% transform car to new position with respect to car's reference frame
car_position = transformation_problem1 * car_position;
% convert new position to gloabl reference frame
car_position = transformation_start * car_position;
plot(car_position(1,:), car_position(2,:), 'r-', 'LineWidth', 2);
legend({'Blue = initial position','Red = position after transformation'},'Location','northeast');